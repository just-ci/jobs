#!/usr/bin/env sh

set -e

rm -rf templates
git clone https://gitlab.com/just-ci/templates.git
cd templates

GIT_TAGS=$(git tag --format "%(refname:strip=1)") # Produces list with 'tags/' prefix
GIT_BRANCHES=$(git branch -r --format "%(refname:strip=3)")
mkdir -p /opt/build/repo/public

for GIT_REF in ${GIT_TAGS} ${GIT_BRANCHES}; do
    printf "[*] Processing ${GIT_REF}..."
    git checkout ${GIT_REF} -q

    FILES=$(grep -rl "\- local: " . --exclude-dir=tests --exclude=.gitlab-ci.yml --exclude=pages-templates.yaml)
    printf "$(echo ${FILES} | wc -w) file(s)..."

    GIT_REF_NAME=${GIT_REF#tags/}
    echo ${FILES} | xargs sed -i "s|- local: \"|- remote: \"${CI_PAGES_URL}/${GIT_REF_NAME}/|g"
    echo ${FILES} | xargs sed -i "s|- local: |- remote: ${CI_PAGES_URL}/${GIT_REF_NAME}/|g"
    cp -r . /opt/build/repo/public/${GIT_REF_NAME}
    rm -rf /opt/build/repo/public/${GIT_REF_NAME}/.git

    git reset --hard -q
    echo "done."
done

echo "[*] Removing files not used by end users..."

rm -rf /opt/build/repo/public/*/tests
find /opt/build/repo/public -type f ! -name "*.yaml" ! -name "*.yml" -delete
find /opt/build/repo/public -type f -name ".gitlab-ci.yml" -delete
find /opt/build/repo/public -type f -name ".yamllint.yml" -delete
find /opt/build/repo/public -type f -name ".pre-commit-config.yaml" -delete
find /opt/build/repo/public -type d -empty -delete

cd /opt/build/repo/public

tree -H '.' --noreport --dirsfirst -T "${PAGES_TEMPLATES_NAME:-Templates}" --charset utf-8 -o index.html .

echo "[*] Publishing $(echo $GIT_TAGS | wc -w) tags and $(echo $GIT_BRANCHES | wc -w) branches to ${CI_PAGES_URL}..."
